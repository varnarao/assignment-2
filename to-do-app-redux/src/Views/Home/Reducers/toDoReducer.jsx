export default function reducer (state = {
  products: [],
  fetching: false,
  fetched: false,
  done: false,
  error: null,
  search: null,
  searchStatus: false,
  page: null
}, action) {
  switch (action.type) {
    case 'FETCH_PRODUCT_PENDING': {
      return { ...state, fetching: true }
    }
    case 'FETCH_PRODUCT_REJECTED': {
      return { ...state, fetching: false, error: action.payload }
    }
    case 'FETCH_PRODUCT_FULFILLED': {
      return {
        ...state,
        fetching: false,
        fetched: true,
        done: true,
        products: action.payload,
        searchStatus: false,
        page: 'fetch'
      }
    }
    case 'EDIT_DATA_PENDING': {
      return {
        ...state
      }
    }
    case 'EDIT_DATA_REJECTED': {
      return {
        ...state
      }
    }
    case 'EDIT_DATA_FULFILLED': {
      const prod = { ...state.products }
      const index = state.products.data.findIndex(prod => prod.id === action.payload.id.toString())
      prod.data[index] = action.payload.res.data
      console.log(typeof action.payload.id)
      return {
        ...state,
        fetched: false,
        products: prod
      }
    }
    case 'DELETE_DATA_PENDING': {
      return {
        ...state
      }
    }
    case 'DELETE_DATA_REJECTED': {
      return {
        ...state
      }
    }
    case 'DELETE_DATA_FULFILLED': {
      const prod = { ...state.products }
      const index = state.products.data.findIndex(prod => prod.id === action.payload.id.toString())
      prod.data.splice(index, 1)
      console.log(index)
      return {
        ...state,
        done: true,
        fetched: false,
        products: prod
      }
    }
    case 'SEARCH_FILTER_DATA_FULFILLED': {
      // console.log(JSON.stringify(action.payload))
      return {
        ...state,
        products: action.payload.res,
        search: action.payload.data,
        searchStatus: true,
        page: (action.payload.count === 0) ? 'reset' : 'search'
      }
    }
    default : {
      return state
    }
  }
}
