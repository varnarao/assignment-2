import { makeStyles } from '@material-ui/core/styles'
import styled from 'styled-components'

const style = styled.span`
    height: 30.5px;
    width: 30.5px;
    background-color: #404b69;
    border-radius: 50%;
    display: inline-block;
    font-size: 10px;
    font-weight: 600;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: 0.15px;
    color: white;
    text-align: center;
    padding: 8px 0 0 0;
`
export default style

export const useStyles = makeStyles(theme => ({
  badgeColor: {
    backgroundColor: '#e3e4e8'
  },
  card: {
    minWidth: 420,
    marginTop: 50
  },
  container: {
    display: 'Flex',
    justifyContent: 'center'
  },
  actions: {
    float: 'right'
  },
  titleStyle: {
    marginLeft: '10px'
  }
}))
