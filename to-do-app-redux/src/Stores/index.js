import { applyMiddleware, createStore, combineReducers } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import promise from 'redux-promise-middleware'
import reducer from '../Views/Home/Reducers/toDoReducer'
import addreducer from '../Views/Add/Reducers/addReducer'

const rootReducer = combineReducers({
  ToDo: reducer,
  Add: addreducer
})

const middleware = applyMiddleware(promise, thunk, logger)

export default createStore(rootReducer, middleware)
