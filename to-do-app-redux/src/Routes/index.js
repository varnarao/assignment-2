import AddToDo from '../Views/Add/Container/index'
import ToDo from '../Views/Home/Container/index'

const routes = () => {
  return [
    {
      path: '/',
      exact: true,
      component: ToDo
    },
    {
      path: '/Add',
      component: AddToDo
    }
  ]
}
export default routes
