/* eslint-disable react/prop-types */
import React from 'react'
import clsx from 'clsx'
import { AppBar, Toolbar, Typography, IconButton } from '@material-ui/core'
import { Menu } from '@material-ui/icons'
import styles from './styles'
import Search from './searchView/search'

const DrawerComp = (props) => {
  const classes = styles()
  return (
    <AppBar
      position="fixed"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: props.open
      })}
    >
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={props.openAppBar}
          edge="start"
          className={clsx(classes.menuButton, {
            [classes.hide]: props.open
          })}
        >
          <Menu />
        </IconButton>
        <Typography variant="h6" noWrap>
          <Search />
        </Typography>
      </Toolbar>
    </AppBar>
  )
}
export default DrawerComp
