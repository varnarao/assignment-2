import React from 'react'
import { theme } from './styles/styles'
import { ThemeProvider } from '@material-ui/core'
import './App.css'

import Menu from './Components/MenuDrawerComponent/index'

class App extends React.Component {
  render () {
    return (
      <div className="App">
        <ThemeProvider theme={theme}>
          <Menu />
        </ThemeProvider>
      </div>
    )
  }
}

export default App
