import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  listStyle: {
    textDecoration: 'none',
    listStyleType: 'none',
    color: 'white'
  },
  current: {
    borderLeft: '3px solid green',
    backgroundColor: '#4f2261'
  }
}))

export default useStyles
