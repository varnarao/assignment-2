/* eslint-disable react/prop-types */
import React from 'react'
import clsx from 'clsx'
import { CssBaseline, Divider, IconButton, useTheme, Drawer } from '@material-ui/core'
import { ChevronLeft, ChevronRight } from '@material-ui/icons'
import styles from './style'
import { BrowserRouter as Router } from 'react-router-dom'
import Listview from '../NavLinksComponent/index'
import Appbar from '../AppBarComponent/index'
import SwitchComp from '../SwitchComponent/index'
import Routes from '../../Routes/index'
export default function PersistentDrawerLeft (props) {
  const classes = styles()
  const theme = useTheme()
  const routes = Routes(props.changeVal)
  const [open, setOpen] = React.useState(false)
  const handleDrawerOpen = () => {
    setOpen(true)
  }
  const handleDrawerClose = () => {
    setOpen(false)
  }
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Appbar openAppBar={handleDrawerOpen} open={open} changed={props.changed}/>
      <Router>
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open
            })
          }}
        >
          <div className={classes.toolbar}>
            <h4 className={classes.paper}>To-Do-List</h4>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRight className={classes.paper} /> : <ChevronLeft className={classes.paper} />}
            </IconButton>
          </div>
          <Divider />
          <Listview />
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <SwitchComp routes={routes}/>
        </main>
      </Router>
    </div>
  )
}
