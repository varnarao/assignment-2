import React from 'react'
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { ListAlt, AddBox } from '@material-ui/icons'
import styles from './style'
import { NavLink } from 'react-router-dom'

export default function listV () {
  const classes = styles()
  return (
    <List className={classes.listStyle}>
      <ListItem button component={NavLink} to="/" activeClassName={classes.current} exact>
        <ListItemIcon>
          <ListAlt className={classes.listStyle} />
        </ListItemIcon>
        <ListItemText primary="List" />
      </ListItem>
      <ListItem button component={NavLink} to="/Add" activeClassName={classes.current} exact>
        <ListItemIcon>
          <AddBox className={classes.listStyle}/>
        </ListItemIcon>
        <ListItemText primary="Add" />
      </ListItem>
    </List>
  )
}
