/* eslint-disable react/prop-types */
import React from 'react'
import { TextField } from '@material-ui/core'

const textFields = (props) => {
  return (
    <div>
      <TextField
        id='title'
        label='Title'
        value={props.title}
        onChange={() => props.handleChange()}
        onBlur={() => props.handleBlur()}
        helperText={props.touchedtitle ? props.errorstitle : ''}
        error={props.touchedtitle && Boolean(props.errorstitle)}
        margin='dense'
        variant='outlined'
        fullWidth/>
      <TextField
        id='description'
        label='Description'
        type='description'
        value={props.description}
        onChange={() => props.handleChange()}
        onBlur={() => props.handleBlur()}
        helperText={props.toucheddescription ? props.errorsdescription : ''}
        error={props.toucheddescription && Boolean(props.errorsdescription)}
        multiline
        rows='4'
        margin='dense'
        variant='outlined'
        fullWidth/>
      <TextField
        id='category'
        label='Category'
        value={props.category}
        onChange={() => props.handleChange()}
        onBlur={() => props.handleBlur()}
        error={props.touchedcategory && Boolean(props.errorscategory)}
        margin='dense'
        variant='outlined'
        fullWidth/>
    </div>
  )
}
export default textFields
