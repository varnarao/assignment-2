/* eslint-disable react/prop-types */
import React from 'react'
import { TablePagination } from '@material-ui/core'
import { connect } from 'react-redux'
const tablePagination = (props) => {
  const page = (props.pages === 'reset') ? 0 : props.page
  return (
    <div>
      <TablePagination
        rowsPerPageOptions={[10]}
        component="div"
        count={props.rowLength}
        rowsPerPage={props.rowsPerPage}
        page={page}
        onChangePage={props.handleChangePage}
      />
    </div>
  )
}
const mapStateToProps = state => {
  return {
    pages: state.ToDo.page
  }
}
export default connect(mapStateToProps)(tablePagination)
