import React from 'react'
import DisplayTable from './displayTable'
import { connect } from 'react-redux'
import { getProducts, searchData } from '../Actions/getAction'
import { putDatas,deleteDatas } from '../Actions/editAction'

class  FillTable extends React.Component {
  constructor(){
    super()
    this.state =  { open: false, name: null, desp: null, category: null, id: null,searchPage: 0, page: 0, rowsPerPage: 10 }
  }
  componentDidMount() {
    this.props.dispatch(getProducts(this.state.page+1,this.state.rowsPerPage))
  }
  handlePageChange = (event, newPage) => {
    if(this.props.searchStatus===true && this.props.search!==''){
      this.setState({page: newPage}, () => this.props.dispatch(searchData(this.props.search,this.state.page+1,this.state.rowsPerPage)))
    }else
      this.setState({page: newPage}, () => this.props.dispatch(getProducts(this.state.page+1,this.state.rowsPerPage)))
  }

  changeNameHandler = (event) => this.setState({name: event.target.value})
  changeDespHandler = (event) => this.setState({desp: event.target.value})
  changeCategoryHandler = (event) => this.setState({category: event.target.value})
  clickHandler = (id,name,desp,category) => this.setState({open: true,id: id, name: name, desp: desp, category: category})
  closedHandler= () => this.setState({open: false})
  editHandler = (title,desp,category) => {
    const body = {title: title, description: desp, category: category}
    this.props.dispatch(putDatas(this.state.id, body))
    this.closedHandler()
  }
  deleteHandler = (id) => {
    const value = window.confirm("Are you Sure ?")
  if (value === true) {
    this.props.dispatch(deleteDatas(id))
  } else { alert("ok") }}
  render(){
    const display = this.props.done ? <DisplayTable page={this.state.page} handleChangePage={this.handlePageChange} rowLength={58} deleteToDo = {this.deleteHandler} editToDo={this.editHandler} row = {this.props.prod.data} name = {this.state.name} nameChange = {this.changeNameHandler} desp={this.state.desp} despChange = {this.changeDespHandler} category = {this.state.category} catgChange = {this.changeCategoryHandler} open={this.state.open} clicked={this.clickHandler} close={this.closedHandler} /> : "Loading..."
  return (
    <div className="App">
      {display}
    </div>
  )}}
const mapStateToProps =  state => {
  return{
    prod: state.ToDo.products, done : state.ToDo.done, search: state.ToDo.search, searchStatus: state.ToDo.searchStatus
  }
}
export default connect(mapStateToProps)(FillTable);
