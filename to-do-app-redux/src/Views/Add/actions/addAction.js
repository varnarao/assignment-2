import { post } from '../../../Utils/apiCallMethods'
import APIURL from '../../../Constants/apiConstants'

export function postData (body) {
  return {
    type: 'ADD_ACTION',
    payload: post(`${APIURL.TASKAPI}`, body)
  }
}
