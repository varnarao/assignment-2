import axios from 'axios'

export function get (url, headers = {}, payload = {}) {
  return axios.get(url, { 'Access-Control-Allow-Headers': '*' })
}

export function put (url, headers = {}, payload = {}) {
  return axios.put(url, payload, headers)
  // return axios.put(url, payload, headers)
}

export function post (url, payload = {}, headers = {}) {
  return axios.post(url, payload, headers)
}

export function deleteData (url, headers = {}, payload = {}) {
  return axios.delete(url, payload, headers)
}
