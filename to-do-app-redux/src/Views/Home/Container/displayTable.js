/* eslint-disable react/prop-types */
import React from 'react'
import Stylespan, { useStyles } from './styles'
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Badge, Card, CardContent } from '@material-ui/core'
import { Edit, Delete } from '@material-ui/icons'
import HandleModal from '../../../Components/EditModalComponent/index'
import { connect } from 'react-redux'
import TablePagination from './tablePagination'

const DisplayTable = props => {
  const classes = useStyles()
  return (
    <div>
      <Card className={classes.card} variant="outlined">
        <CardContent>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align='right'>Description</TableCell>
                  <TableCell align='right'>Category</TableCell>
                  <TableCell align='right'>Edit</TableCell>
                  <TableCell align='right'>Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {props.prod.data.map(row => (
                  <TableRow key={row.id}>
                    <TableCell component='th'> <Badge badgeContent={4} color="secondary" anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'left'
                    }}>
                      <Stylespan>TMH</Stylespan></Badge><span className={classes.titleStyle}>{row.title}</span></TableCell>
                    <TableCell align='right'>{row.description.length > 11 ? row.description.substr(0, 11) + '...' : row.description}</TableCell>
                    <TableCell align='right'>{row.category}</TableCell>
                    <TableCell align='right'><Edit color="primary" onClick={() => props.clicked(row.id, row.title, row.description, row.category)}/></TableCell>
                    <TableCell align='right'><Delete color="primary" onClick={() => props.deleteToDo(row.id)} /></TableCell>
                    <TableCell align='right'></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>
      <TablePagination rowLength={58} rowsPerPage={10} page={props.page} handleChangePage={props.handleChangePage} />
      <HandleModal editToDo={props.editToDo} name={props.name} nameChange={props.nameChange} desp={props.desp} despChange={props.despChange} category={props.category} catgChange={props.catgChange} open={props.open} close={props.close}/>
    </div>
  )
}
const mapStateToProps = state => {
  return {
    prod: state.ToDo.products
  }
}
export default connect(mapStateToProps)(DisplayTable)
