/* eslint-disable react/prop-types */
import React from 'react'
import { withFormik } from 'formik'
import * as Yup from 'yup'
import { withStyles, Card, CardContent, TextField, Button } from '@material-ui/core'
import styles from './style'
import { postData } from '../../actions/addAction'
import { connect } from 'react-redux'

const form = props => {
  const {
    classes,
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit
  } = props

  return (
    <div className={classes.container}>
      <form onSubmit={handleSubmit}>
        <Card className={classes.card}>
          <CardContent>
            <TextField
              id='title'
              label='Title'
              value={values.title}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.title ? errors.title : ''}
              error={touched.title && Boolean(errors.title)}
              margin='dense'
              variant='outlined'
              fullWidth
            />
            <TextField
              id='description'
              label='Description'
              type='description'
              value={values.description}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.description ? errors.description : ''}
              error={touched.description && Boolean(errors.description)}
              multiline
              rows='4'
              margin='dense'
              variant='outlined'
              fullWidth
            />
            <TextField
              id='category'
              label='Category'
              value={values.category}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.category ? errors.category : ''}
              error={touched.category && Boolean(errors.category)}
              margin='dense'
              variant='outlined'
              fullWidth
            />
          </CardContent>
          <Button type='submit' color='primary' disabled={isSubmitting}>
              SUBMIT</Button> <br/><br/>
        </Card>
      </form>
    </div>
  )
}

const Form = withFormik({
  mapPropsToValues: ({
    title,
    category,
    description
  }) => {
    return {
      title: title || '',
      category: category || '',
      description: description || ''
    }
  },

  validationSchema: Yup.object().shape({
    title: Yup.string().required('Required'),
    category: Yup.string().required('Required'),
    description: Yup.string().required('description is required')
  }),

  handleSubmit: (values, { props, setSubmitting, resetForm }) => {
    setSubmitting(false)
    resetForm()
    alert('Inserted')
    props.dispatch(postData(values))
  }
})(form)

export default connect()(withStyles(styles)(Form))
