import { put, deleteData } from '../../../Utils/apiCallMethods'
import APIURL from '../../../Constants/apiConstants'

export function putDatas (id, body) {
  return dispatch => {
    dispatch({ type: 'EDIT_DATA' })
    put(`${APIURL.TASKAPI}/${id}`, {}, body)
      .then(res => {
        dispatch({ type: 'EDIT_DATA_FULFILLED', payload: { res: res, id: id } })
      })
      .catch(err => {
        dispatch({ type: 'EDIT_DATA_REJECTED', payload: err.message })
      })
  }
}

export function deleteDatas (id) {
  return dispatch => {
    dispatch({ type: 'DELETE_DATA' })
    deleteData(`${APIURL.TASKAPI}/${id}`)
      .then(res => {
        dispatch({ type: 'DELETE_DATA_FULFILLED', payload: { res: res, id: id } })
      })
      .catch(err => {
        dispatch({ type: 'DELETE_DATA_REJECTED', payload: err.message })
      })
  }
}
