import { get } from '../../../Utils/apiCallMethods'
import APIURL from '../../../Constants/apiConstants'
export function getProducts (page, limit) {
  return {
    type: 'FETCH_PRODUCT',
    payload: get(`${APIURL.TASKAPI}?page=${page}&limit=10`)
  }
}

export function searchData (data, page, count) {
  return dispatch => {
    dispatch({ type: 'SEARCH_FILTER_DATA' })
    get(`${APIURL.TASKAPI}?search=${data}&page=${page}&limit=10`)
      .then(res => {
        dispatch({ type: 'SEARCH_FILTER_DATA_FULFILLED', payload: { res: res, data: data, count: count } })
      })
      .catch(err => {
        dispatch({ type: 'SEARCH_FILTER_DATA_REJECTED', payload: err.message })
      })
  }
}
