/* eslint-disable react/prop-types */
import React from 'react'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import Typography from '@material-ui/core/Typography'
import styles from './style'
import { ThemeProvider } from '@material-ui/core'
import { searchData } from '../../../Views/Home/Actions/getAction'
import { connect } from 'react-redux'
import { theme } from '../../../styles/styles'

const search = (props) => {
  let searchCount = 0
  const filterFunction = (event) => {
    searchCount = (event.target.value === '') ? 0 : searchCount++
    props.dispatch(searchData(event.target.value, 1, searchCount))
  }
  const classes = styles()
  return (
    <div className={classes.rootsearch}>
      <ThemeProvider theme={theme}>
        <Typography className={classes.title} variant="h6" noWrap />
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder="Search…"
            onChange={(event) => filterFunction(event)}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput
            }}
            inputProps={{ 'aria-label': 'search' }}
          />
        </div>
      </ThemeProvider>
    </div>
  )
}
export default connect(null, null)(search)
